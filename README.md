# ARKBeizli ("Archival Resource Key Beizli")

## Description

ARKBeizli (sounds a bit like "ArkPyCli") is a lightweight command line tool
written in Python to create ("mint") ARK (Archival Resource Key) identifiers.
This script was born of ZHB Luzern and first used in the Central Swiss cultural
heritage platform ZentralGut as a successor of ArkeType.ch.

"Beizli" is reminiscent of the Swiss German word "Beiz", meaning a comfortable,
traditional inn, and "li" makes it small and cosy - like this script - a small
and cosy tool to create and maintain your Archival Resource Keys.

## Setup

* Clone this repository and run setup.py: `python3 setup.py`

```bash

Please enter the filename for the SQLite database (default: arkDb.sqlite): myDatabaseName.sqlite
Enter your NAAN [default: 99999]:   
Enter your default Shoulder [for test naan use fk4]: fk4
Enter your default Ark-Structure [default: seddek]:
Enter your n2t-User (optional): 
Enter your n2t-Password (optional): 
Enter your n2t-URL (default: https://n2t.net): 
************************************
Database 'database/myDatabaseName.sqlite' has been successfully created with table 't_ark'.
Your given configuration variables are saved in '.env'.
************************************
Use ARKBeizli now, go to the your favourite ARKBeizli, grab a coffee and type: python3 main.py --help
```

The setup asks for some configurations and creates the local sqlite-Database and a `.env` file storing some configurations as given above.

## Usage

use: `python3 main.py --help`

```bash
Welcome to the ARKBeizli - Mint, bind and maintain your Archival Resource Keys (ARK)

options:
  -h, --help            show this help message and exit
  -m, --mint            With flag -m you mint a new Ark (with duplicate check in the local filebase and against n2t.net)
  -b, --bind            With flag -b your ark will be bound against n2t.net
  -o [OUTPUT], --output [OUTPUT]
                        Exports your ARKs and Target_URLs to the given filename (default: t_ark.csv)
  --target TARGET, -t TARGET
                        Target URL
  --naan NAAN, -n NAAN  Your NAAN
  --shoulder SHOULDER, -s SHOULDER
                        Shoulder as first pat of the AKR
  --noidTemplate NOIDTEMPLATE, -c NOIDTEMPLATE
                        Defines the structure (digits, characters) of the ARK identifier
```

### Mint an ARK

The command `python3 main.py -m` creates a new ark and stores it in the local database.

If you add a `--target {TARGET_URL}` : `python3 main.py -m -t https://YOUR.TARGET.URL` - both the new ARK and target are stored in the database

### Mint and bind an ARK

If you have credentials for n2t.net you can bind a new created ARK to n2t:

`python3 main.py -mb -t https://YOUR.TARGET.URL`

### Export your local filedatabase to a csv

`python3 main.py -o allMyArksOutputFileName.csv`

## Credits

`pymint.py` is forked from https://github.com/vt-digital-libraries-platform/NOID-mint (under MIT-License)

All other scripts are developed by [ZHB Luzern](https://zhbluzern.ch) and published under the [MIT-0 No Attribution License](LICENSE)
