import argparse
import n2t.n2tbind as n2t

# Create ArgumentParser
parser = argparse.ArgumentParser(description='Bind Metadata to an ARK', usage="python3 run_n2tMdUpdate.py --ark --mdType --mdValue")
parser.add_argument('--ark', '-a', required=True, help='ARK die aktualisiert werden soll', )
parser.add_argument('--mdType', '-m', required=True, help='Metadatum das aktulaisiert werden soll', )
parser.add_argument('--mdValue', '-v', required=True, help='Wert des zu aktualisierenden Metadatums', )
args = parser.parse_args()

#Parse Arguments
if args.ark:
    ark = args.ark
else:
    ark = ""
if args.mdType:
    mdType = args.mdType
else:
    mdType = ""    
if args.mdValue:
    mdValue = args.mdValue
else:
    mdValue = ""     

#Bind Metadata to ark in n2t
n2t.setArkMd(ark, mdType, mdValue)