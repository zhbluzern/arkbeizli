import src.n2t.n2tbind as n2t
import src.pymint.pymint as ark

def mintArk(arkDb, naan, shoulder, noidTemplate):

    alreadyUsedArk = 1
    arkLoopCounter = 0
    #Loop through new ark until a new Ark (alreadyUsedArk==None) is found fo given naan/shoulder
    while alreadyUsedArk != None:
        #create a new ark like ark:/63274/bz1s46rh
        newArk = ark.mint(template=f'{shoulder}.{noidTemplate}', n=None, scheme='ark:/', naa=naan)
        #to avoid duplicate arks check if "new" ark is already in use:
        alreadyUsedArk = arkDb.arkInDb(newArk) #check if minted ARK exists in local filebase
        #print(f"already used ark: {alreadyUsedArk}")
        if n2t.existsArk(newArk) == True: #check if newly minted ARK exists in n2t
            alreadyUsedArk = 1
        #print(f"already used ark: {alreadyUsedArk}")
        arkLoopCounter += 1
    print(f"{str(arkLoopCounter)} try(s): {newArk}")
    return newArk