import sqlite3 as db
import pandas as pd

class sqlark:

    # Initialize sqlark-Class
    def __init__(self, databasePath):
        self.con = db.connect(databasePath)
        
    def arkInDb(self, ark):
        cur = self.con
        res = cur.execute("SELECT * FROM t_ark WHERE ark = ?", (ark,))
        return (res.fetchone())
    
    def targetinDb(self, target):
        cur = self.con
        res = cur.execute("SELECT * FROM t_ark WHERE target = ?", (target,))
        return (res.fetchone())
       
    def storeArk(self, ark, target):
        cur = self.con
        data = [ark, target]
        sql = "INSERT  OR IGNORE INTO t_ark VALUES (?, ?)"
        cur.execute(sql,data)
        self.con.commit()

    def updateArk(self, ark, target):
        cur = self.con
        data = [target, ark]
        sql = "UPDATE t_ark SET target = ? WHERE ark = ?"
        cur.execute(sql, data)
        self.con.commit()

    def export2CSV(self, csv_file_path="t_ark.csv"):
        sql = "SELECT * FROM t_ark;"
        db_df = pd.read_sql_query(sql, self.con)
        db_df.to_csv(csv_file_path, index=False)