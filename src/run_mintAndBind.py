import pymint.pymint as ark
import sqlark.sqlark
import argparse
import n2t.n2tbind as n2t
import os
from dotenv import load_dotenv

# Load environment variables
load_dotenv()

# Erstelle den ArgumentParser
parser = argparse.ArgumentParser(description='Mint an ARK', usage="python3 run.py --target={TARGET_URL} --naan={YOUR_NAAN} --shoulder={YOUR_SHOULDER}")
# Füge das Argument 'shoulder' hinzu
parser.add_argument('--target', '-t', required=True, help='Target URL auf die ARK zeigen soll' )
parser.add_argument('--naan', '-n', required=False, help='NAAN als erster fixer Teil einer ARK')
parser.add_argument('--shoulder', '-s', required=False, help='Shoulder als zweiter, fixer Teil einer ARK')
parser.add_argument('--noidTemplate', '-c', required=False, help='Defines the structure (digits, characters) of the ARK identifier')
# Parse die Argumente
args = parser.parse_args()
# Zugriff auf das Argument 'shoulder'
print(f"Empfangene Shoulder: {args.shoulder}")
if args.shoulder:
    shoulder = args.shoulder
else:
    shoulder = os.getenv("shoulder")
# Zugriff auf das Argument 'target'
print(f"Empfangenes Target: {args.target}")
if args.target:
    target = args.target
else:
    target = ""
# Zugriff auf das Argument 'NAAN'    
if args.naan:
    naan = args.naan
else:
    naan = os.getenv("naan") #Test-NAAN
#Zugriff auf das Argument "Schema"
if args.noidTemplate:
    noidTemplate = args.noidTemplate
else:
    noidTemplate = os.getenv("noidTemplate")

#Initialize ArkDB-Class
arkDb = sqlark.sqlark.sqlark(os.getenv("database"))

alreadyUsedArk = 1
arkLoopCounter = 0
#Loop through new ark until a new Ark (alreadyUsedArk==None) is found fo given naan/shoulder
while alreadyUsedArk != None:
    #create a new ark like ark:/63274/bz1s46rh
    newArk = ark.mint(template=f'{shoulder}.{noidTemplate}', n=None, scheme='ark:/', naa=naan)
    #to avoid duplicate arks check if "new" ark is already in use:
    alreadyUsedArk = arkDb.arkInDb(newArk) #check if minted ARK exists in local filebase
    #print(f"already used ark: {alreadyUsedArk}")
    if n2t.existsArk(newArk) == True: #check if newly minted ARK exists in n2t
        alreadyUsedArk = 1
    #print(f"already used ark: {alreadyUsedArk}")
    arkLoopCounter += 1

print(f"{str(arkLoopCounter)} try(s): {newArk}")
#store ARK and target in Database
arkDb.storeArk(newArk, target)

# bind ARK to n2t
n2t.bindArk(newArk, target)
print(f"ARK https://n2t.net/{newArk}?? bound in n2t: {n2t.existsArk(newArk)}")