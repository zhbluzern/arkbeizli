﻿import requests
from dotenv import load_dotenv
import os
import warnings
from urllib3.exceptions import InsecureRequestWarning

# Surpress Warnings from n2t
warnings.filterwarnings('ignore', category=InsecureRequestWarning)

# Load environment variables
load_dotenv()

# Replace these variables with your actual username and password
username = os.getenv("username")
password = os.getenv("password")
url = os.getenv("n2turl")
#print(username)
#print(password)

def getReadme():
    # Send a GET request
    response = requests.get(f"{url}/a/{username}/b?help%20readme", auth=(username, password), verify=False)

    # Check if the request was successful
    if response.status_code == 200:
        print (response.text)
    else:
        print ("Failed to retrieve data: Status code", response.status_code)

def bindArk(ark, target):    
    postData = f"{ark}.set _t \"{target}\""
    response = requests.post(f"{url}/a/{username}/b?-", auth=(username, password), verify=False, data=postData)
    return response.text

def bindArkWithMd(ark, target, who="",title="", when="", docType=""):
    title = title.replace('"', '\\"')
    who = who.replace('"', '\\"')
    postData = f"{ark}.set _t \"{target}\""
    postData = f"{postData}\n{ark}.set how \"(:mtype {docType})\""
    postData = f"{postData}\n{ark}.set who \"{who}\""
    postData = f"{postData}\n{ark}.set what \"{title}\""
    #postData = f"{postData}\n{ark}.set dc.title \"{title}\""
    postData = f"{postData}\n{ark}.set when {when}"
    
    print(postData)
    headers = {'Content-Type': 'text/plain; charset=utf-8'}
    response = requests.post(f"{url}/a/{username}/b?-", auth=(username, password), verify=False, data=postData.encode('utf-8'), headers=headers)
    return response.text

def setArkMd(ark, mdString, mdValue):
    #Function to set a specific Metadata to an ARK
    mdValue = mdValue.replace('"', '\\"')
    postData = f"{ark}.set {mdString} \"{mdValue}\""
    print(postData)
    headers = {'Content-Type': 'text/plain; charset=utf-8'}
    response = requests.post(f"{url}/a/{username}/b?-", auth=(username, password), verify=False, data=postData.encode('utf-8'), headers=headers)
    return response.text

def existsArk(ark):
    response = requests.get(f"{url}/a/{username}/b?{ark}.exists", auth=(username, password), verify=False)
    print(f"{url}/a/{username}/b?{ark}.exists")
    if 'exists: 1' in response.text:
        return True
    elif 'exists: 0' in response.text:
        return False
    else:
        return None

def removeArkMd(ark,mdString):
    #Function to remove a certain md value in an ark
    response = requests.get(f"{url}/a/{username}/b?{ark}.rm {mdString}", auth=(username, password), verify=False)
    return response.text

def purgeArk(ark):
    #Handle with care! This function purge the whole ARK permanetly!
    response = requests.get(f"{url}/a/{username}/b?{ark}.purge", auth=(username, password), verify=False)
    return response.text

if __name__ == '__main__':
    #testArk = "ark:/99999/fk3h2580"
    #print(existsArk(testArk))
    resp = setArkMd(testArk, "what", "Bürgenstock")
    print(resp)
    #print(existsArk(testArk))
    #print(purgeArk("ark:/99999/fk3170k1"))
    #getReadme()