import src.run_mint as mint
import src.sqlark.sqlark as sqlark
import src.n2t.n2tbind as n2t
import argparse
import os
from dotenv import load_dotenv

# Load environment variables
load_dotenv()

# Create ArgumentParser
parser = argparse.ArgumentParser(description='Welcome to the ARKBeizli - Mint, bind and maintain your Archival Resource Keys (ARK)', usage="python3 run.py --target={TARGET_URL} --naan={YOUR_NAAN} --shoulder={YOUR_SHOULDER}")

parser.add_argument('-m', '--mint', action='store_true', help='With flag -m you mint a new Ark (with duplicate check in the local filebase and against n2t.net)')
parser.add_argument('-b', '--bind', action='store_true', help='With flag -b your ark will be bound against n2t.net')
parser.add_argument('-o', '--output', type=str, nargs='?', const='t_ark.csv', default=False,  help='Exports your ARKs and Target_URLs to the given filename (default: t_ark.csv)')

parser.add_argument('--target', '-t', type=str, help='Target URL' )
parser.add_argument('--naan', '-n', required=False, help='Your NAAN')
parser.add_argument('--shoulder', '-s', required=False, help='Shoulder as first pat of the ARK')
parser.add_argument('--noidTemplate', '-c', required=False, help='Defines the structure (digits, characters) of the ARK identifier')

# Parse the Argumentes
args = parser.parse_args()
print(args)
if not args.mint and not args.bind and not args.output:
    parser.error("No Parameter given")

if args.mint and args.bind and not args.target:
    parser.error("To bind an ARK against n2t a target-URL is needed!")
# Access Argument 'shoulder'
if args.shoulder:
    shoulder = args.shoulder
else:
    shoulder = os.getenv("shoulder")
# Access  Argument 'target'
if args.target:
    target = args.target
else:
    target = ""
# Access Argument 'NAAN'    
if args.naan:
    naan = args.naan
else:
    naan = os.getenv("naan") #Test-NAAN
#Access Argument "Schema"
if args.noidTemplate:
    noidTemplate = args.noidTemplate
else:
    noidTemplate = os.getenv("noidTemplate")

#Initialize ArkDB-Class
arkDb = sqlark.sqlark(os.getenv("database"))

#Get NewArk
if args.mint:
    newArk = mint.mintArk(arkDb,naan,shoulder,noidTemplate)
    print(newArk)
    #store ARK and target in Database
    arkDb.storeArk(newArk, target)

#Bind ARK
if args.bind:
    # bind ARK to n2t
    n2t.bindArk(newArk, target)
    print(f"ARK https://n2t.net/{newArk}?? bound in n2t: {n2t.existsArk(newArk)}")

#Export t_ark to CSV:
if args.output:
    arkDb.export2CSV(args.output)
    print(f"All stored ARKs and their targets are exported to '{args.output}'")