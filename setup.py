import os
import sqlite3
import subprocess
import sys

def install(package):
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])


def main():
    required_modules = ["requests", "python-dotenv", "pandas"]
    for module in required_modules:
        try:
            __import__(module)
        except ImportError:
            install(module)

    # Set default database name
    default_db_name = "arkDb.sqlite"

    # Prompt for the database filename with default name
    db_name = input(f"Please enter the filename for the SQLite database (default: {default_db_name}): ") or default_db_name
    # Check if the filename has an extension, append '.sqlite' if not
    if '.' not in db_name:
        db_name += ".sqlite"

    # Additional .env variables
    naan = input("Enter your NAAN [default: 99999]: ") or "99999"
    shoulder = input("Enter your default Shoulder [for test naan use fk4]: ") or ""
    noidTemplate = input("Enter your default Ark-Structure [default: seddek]: ") or "seddek"
    n2t_user = input("Enter your n2t-User (optional): ") or ""
    n2t_password = input("Enter your n2t-Password (optional): ") or ""
    n2t_url = input("Enter your n2t-URL (default: https://n2t.net): ") or "https://n2t.net"

    # Create path for the database directory
    db_dir = os.path.join(os.getcwd(), 'database')
    if not os.path.exists(db_dir):
        os.makedirs(db_dir)

    # Create database file path
    db_path = f"{db_dir}/{db_name}"

    # Create database and table
    conn = sqlite3.connect(db_path)
    cursor = conn.cursor()
    cursor.execute("CREATE TABLE IF NOT EXISTS t_ark(ark TEXT, target TEXT);")
    conn.commit()
    conn.close()

    # Save path and other variables in the .env file
    env_path = os.path.join(os.getcwd(), '.env')
    with open(env_path, 'w') as env_file:
        env_file.write("#sqlite configuration\n")
        env_file.write(f"database=database/{db_name}\n")
        env_file.write("\n#ark-configuration\n")
        env_file.write(f"naan={naan}\n")
        env_file.write(f"shoulder={shoulder}\n")
        env_file.write(f"noidTemplate={noidTemplate}\n")
        env_file.write("\n#n2t configurations\n")
        if n2t_user:
            env_file.write(f"username={n2t_user}\n")
        if n2t_password:
            env_file.write(f"password={n2t_password}\n")
        if n2t_url:
            env_file.write(f"n2turl={n2t_url}\n")
    
    print(f"************************************")
    print(f"Database 'database/{db_name}' has been successfully created with table 't_ark'.")
    print(f"Your given configuration variables are saved in '.env'.")
    print(f"************************************")
    print(f"Use ARKPyCli now, go to the your favourite ARKBeizli, grab a coffee and type: python3 main.py --help")

if __name__ == "__main__":
    main()
